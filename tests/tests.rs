extern crate nsgrid;
use approx::assert_ulps_eq;
#[cfg(feature = "from-chemfiles")]
use chemfiles as cfl;

#[test]
fn errors() {
    let frame = nsgrid::frame::Frame::new(
        nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true; 3]),
        vec![],
    );

    assert!(nsgrid::NSGrid::new(frame.clone(), 1.0).is_ok());
    assert!(nsgrid::NSGrid::new(frame.clone(), 10.0).is_ok());
    assert!(nsgrid::NSGrid::new(frame.clone(), 10.1).is_err());
    assert!(nsgrid::NSGrid::new(frame, 1000.0).is_err());

    let frame = nsgrid::frame::Frame::new(nsgrid::cell::Cell::infinite(), vec![[1.0, 2.0, 3.0]]);

    assert!(nsgrid::NSGrid::new(frame.clone(), 5.0).is_ok());
    assert!(nsgrid::NSGrid::new(frame.clone(), 1000.0).is_ok());

    let nsgrid = nsgrid::NSGrid::new(frame, 5.0).unwrap();
    assert!(nsgrid.get_neighbours(0).is_ok());
    assert!(nsgrid.get_neighbours(1).is_err());

    let frame = nsgrid::frame::Frame::new(
        nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [false; 3]),
        vec![[0.0; 3], [10.0, 20.0, 30.0]],
    );

    let mut nsgrid = nsgrid::NSGrid::new(frame, 3.0).unwrap();
    assert!(nsgrid.update_position(0, [-1.0, 0.0, 0.0]).is_err());
    assert!(nsgrid.update_position(1, [100.0, 0.0, 0.0]).is_err());
    assert!(nsgrid.add_position([0.0, 0.0, -10.0]).is_err());
    assert!(nsgrid.add_position([0.0, 100.0, 0.0]).is_err());
}

#[test]
fn preselection() {
    let cell = nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true; 3]);
    let frame = nsgrid::frame::Frame::new(
        cell,
        vec![[0.0, 0.0, 0.0], [1.0, 2.0, 3.0], [1.0, 2.0, 3.0]],
    );

    let nsgrid = nsgrid::NSGrid::with_selection(frame, 5.0, vec![1, 2]).unwrap();
    assert!(nsgrid.get_neighbours(0).is_err());
    let nsg_result = nsgrid.get_neighbours(1).unwrap();
    assert_eq!(nsg_result.len(), 1);
    assert_eq!(nsg_result[0], 2);
}

#[test]
fn pbc() {
    let mut positions = vec![
        [1.0, 1.0, 9.0],
        [9.0, 1.0, 9.0],
        [9.0, 1.0, 1.0],
        [9.0, 9.0, 1.0],
    ];

    {
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 10.0, 10.0], [true; 3]);
        let frame = nsgrid::frame::Frame::new(cell, positions.clone());

        let nsgrid = nsgrid::NSGrid::new(frame, 4.0).unwrap();
        let mut nsg_result = nsgrid.get_neighbours(0).unwrap();
        nsg_result.sort_unstable();
        assert_eq!(nsg_result.len(), 3);
        assert_eq!(nsg_result[0], 1);
        assert_eq!(nsg_result[1], 2);
        assert_eq!(nsg_result[2], 3);
    }

    {
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 10.0, 10.0], [true, false, false]);
        let frame = nsgrid::frame::Frame::new(cell, positions.clone());

        let nsgrid = nsgrid::NSGrid::new(frame, 4.0).unwrap();
        let nsg_result = nsgrid.get_neighbours(0).unwrap();
        assert_eq!(nsg_result.len(), 1);
        assert_eq!(nsg_result[0], 1);
    }

    {
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 10.0, 10.0], [false, true, false]);
        let frame = nsgrid::frame::Frame::new(cell, positions.clone());

        let nsgrid = nsgrid::NSGrid::new(frame, 4.0).unwrap();
        let nsg_result = nsgrid.get_neighbours(2).unwrap();
        assert_eq!(nsg_result.len(), 1);
        assert_eq!(nsg_result[0], 3);
    }

    {
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 10.0, 10.0], [false, false, true]);
        let frame = nsgrid::frame::Frame::new(cell, positions.clone());

        let nsgrid = nsgrid::NSGrid::new(frame, 4.0).unwrap();
        let nsg_result = nsgrid.get_neighbours(2).unwrap();
        assert_eq!(nsg_result.len(), 1);
        assert_eq!(nsg_result[0], 1);
    }

    {
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 10.0, 10.0], [false; 3]);
        positions.push([1.0, 1.0, 11.0]);
        let frame = nsgrid::frame::Frame::new(cell, positions);

        let nsgrid = nsgrid::NSGrid::new(frame, 3.0).unwrap();
        let mut nsg_result = nsgrid.get_neighbours(0).unwrap();
        nsg_result.sort_unstable();
        assert_eq!(nsg_result.len(), 1);
        assert_eq!(nsg_result[0], 4);
    }
}

#[test]
fn position_manipulation() {
    let cell = nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true, true, false]);
    let frame = nsgrid::frame::Frame::new(cell, vec![[0.0, 0.0, 0.0]]);

    let mut nsgrid = nsgrid::NSGrid::new(frame, 5.0).unwrap();
    let nsg_result = nsgrid.get_neighbours(0).unwrap();
    assert_eq!(nsg_result.len(), 0);

    nsgrid.add_position([1.0, 2.0, 3.0]).unwrap();
    nsgrid.add_position([5.0, 10.0, 5.0]).unwrap();
    let nsg_result = nsgrid.get_neighbours(0).unwrap();
    assert_eq!(nsg_result.len(), 1);
    assert_eq!(nsg_result[0], 1);

    nsgrid.update_position(2, [2.0, 1.0, 3.0]).unwrap();
    let mut nsg_result = nsgrid.get_neighbours(0).unwrap();
    nsg_result.sort_unstable();
    assert_eq!(nsg_result.len(), 2);
    assert_eq!(nsg_result[0], 1);
    assert_eq!(nsg_result[1], 2);
}

fn approx_eq_vec(x: &[f64], x_ref: &[f64], e: f64) -> bool {
    if x.len() != x_ref.len() {
        eprintln!("Vector lengths differ: {:?}, {:?}", x, x_ref);
        return false;
    }
    if !x
        .iter()
        .zip(x_ref.iter())
        .all(|(x, x_ref)| (x - x_ref).abs() < e)
    {
        eprintln!("Vector components differ: {:?}, {:?}", x, x_ref);
        return false;
    }
    true
}

#[test]
fn cell_wrap() {
    {
        let mut positions = vec![
            [0.0, 0.0, 0.0],
            [10.0, 0.0, 0.0],
            [0.0, 20.0, 0.0],
            [0.0, 0.0, 30.0],
            [5.0, 0.0, 0.0],
            [-5.0, 0.0, 0.0],
            [0.0, 10.0, 0.0],
            [0.0, -10.0, 0.0],
            [0.0, 0.0, 15.0],
            [0.0, 0.0, -15.0],
        ];
        let positions_expected = vec![
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [-5.0, 0.0, 0.0],
            [-5.0, 0.0, 0.0],
            [0.0, -10.0, 0.0],
            [0.0, -10.0, 0.0],
            [0.0, 0.0, -15.0],
            [0.0, 0.0, -15.0],
        ];
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true; 3]);
        positions.iter_mut().for_each(|x| cell.wrap(x));
        positions
            .into_iter()
            .zip(positions_expected.into_iter())
            .for_each(|(x, x_ref)| assert_ulps_eq!(x.as_ref(), x_ref.as_ref()));
    }

    {
        let mut positions = vec![
            [0.0, 0.0, 0.0],
            [10.0, 0.0, 0.0],
            [5.0, 0.0, 0.0],
            [-5.0, 0.0, 0.0],
        ];
        let positions_expected = vec![
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [-5.0, 0.0, 0.0],
            [-5.0, 0.0, 0.0],
        ];
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true, false, false]);
        positions.iter_mut().for_each(|x| cell.wrap(x));
        positions
            .into_iter()
            .zip(positions_expected.into_iter())
            .for_each(|(x, x_ref)| assert_ulps_eq!(x.as_ref(), x_ref.as_ref()));
    }

    {
        let mut positions = vec![
            [0.0, 0.0, 0.0],
            [10.0, 0.0, 0.0],
            [0.0, 20.0, 0.0],
            [0.0, 0.0, 30.0],
        ];
        let positions_expected = vec![
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
            [0.0, 0.0, 0.0],
        ];
        let cell = nsgrid::cell::Cell::triclinic([10.0, 20.0, 30.0], [90.0; 3], [true; 3]);
        positions.iter_mut().for_each(|x| cell.wrap(x));
        positions
            .into_iter()
            .zip(positions_expected.into_iter())
            .for_each(|(x, x_ref)| assert!(approx_eq_vec(&x, &x_ref, 1e-12)));
    }
}

#[cfg(all(feature = "from-chemfiles", test))]
mod from_chemfiles {
    use super::*;

    #[test]
    fn orthorhombic() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_orthorhombic_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn orthorhombic_positions() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_orthorhombic_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        let positions = frame.positions();
        for (i, pos) in positions.iter().enumerate().step_by(100) {
            let mut nsg_result = nsgrid.get_position_neighbours(&pos).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(nsgrid.has_position_neighbours(&pos).unwrap());
            assert_eq!(nsg_result.len() - 1, matches.len());
        }
    }

    #[test]
    fn orthorhombic_negative() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_orthorhombic_ld_neg.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn triclinic() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_triclinic_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn triclinic_positions() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_triclinic_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        let positions = frame.positions();
        for (i, pos) in positions.iter().enumerate().step_by(100) {
            let mut nsg_result = nsgrid.get_position_neighbours(&pos).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(nsgrid.has_position_neighbours(&pos).unwrap());
            assert_eq!(nsg_result.len() - 1, matches.len());
        }
    }

    #[test]
    fn triclinic_negative() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_triclinic_ld_neg.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn triclinic_reindex() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/packed_triclinic_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();

        let cell = nsgrid::cell::Cell::from_chemfiles_pbc(&*frame.cell(), [true; 3]);
        let zero_frame = nsgrid::frame::Frame::new(cell, vec![[0.0; 3]; frame.size()]);
        let mut nsgrid = nsgrid::NSGrid::new(zero_frame, 3.0).unwrap();
        for (i, pos) in frame.positions().iter().enumerate() {
            nsgrid.update_position(i, *pos).unwrap();
        }

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn infinite() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/infinite_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        frame.set_cell(&cfl::UnitCell::infinite());
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn infinite_positions() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/infinite_hd.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        frame.set_cell(&cfl::UnitCell::infinite());
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        let positions = frame.positions();
        for (i, pos) in positions.iter().enumerate().step_by(100) {
            let mut nsg_result = nsgrid.get_position_neighbours(&pos).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(nsgrid.has_position_neighbours(&pos).unwrap());
            assert_eq!(nsg_result.len() - 1, matches.len());
        }
    }

    #[test]
    fn infinite_negative() {
        let mut frame = cfl::Frame::new();
        cfl::Trajectory::open("tests/infinite_ld_neg.gro", 'r')
            .unwrap()
            .read(&mut frame)
            .unwrap();
        frame.set_cell(&cfl::UnitCell::infinite());
        let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();

        for i in 0..*[10, frame.size()].iter().min().unwrap() {
            let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
            nsg_result.sort_unstable();

            let mut selection = cfl::Selection::new(&*format!(
                "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
                i
            ))
            .unwrap();
            let matches = selection.evaluate(&frame).unwrap();

            assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
            assert_eq!(nsg_result.len(), matches.len());
        }
    }

    #[test]
    fn errors() {
        let mut frame = cfl::Frame::new();
        frame.set_cell(&cfl::UnitCell::new([10.0, 20.0, 30.0]));

        assert!(nsgrid::NSGrid::new(&frame, 1.0).is_ok());
        assert!(nsgrid::NSGrid::new(&frame, 10.0).is_ok());
        assert!(nsgrid::NSGrid::new(&frame, 10.1).is_err());
        assert!(nsgrid::NSGrid::new(&frame, 1000.0).is_err());

        frame.set_cell(&cfl::UnitCell::infinite());

        assert!(nsgrid::NSGrid::new(&frame, 5.0).is_ok());
        assert!(nsgrid::NSGrid::new(&frame, 1000.0).is_ok());

        frame.add_atom(&cfl::Atom::new("H"), [1.0, 2.0, 3.0], None);
        let nsgrid = nsgrid::NSGrid::new(&frame, 5.0).unwrap();
        assert!(nsgrid.get_neighbours(0).is_ok());
        assert!(nsgrid.get_neighbours(1).is_err());
    }

    #[test]
    fn preselection() {
        let mut frame = cfl::Frame::new();
        frame.set_cell(&cfl::UnitCell::new([10.0, 20.0, 30.0]));

        frame.add_atom(&cfl::Atom::new("H"), [0.0, 0.0, 0.0], None);
        frame.add_atom(&cfl::Atom::new("H"), [1.0, 2.0, 3.0], None);
        frame.add_atom(&cfl::Atom::new("H"), [1.0, 2.0, 3.0], None);

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, 5.0, vec![1, 2]).unwrap();
        assert!(nsgrid.get_neighbours(0).is_err());
        let nsg_result = nsgrid.get_neighbours(1).unwrap();
        assert_eq!(nsg_result.len(), 1);
        assert_eq!(nsg_result[0], 2);
    }

    #[test]
    fn small_cells() {
        for file in ["tests/subcells1.gro", "tests/subcells2.gro"].iter() {
            let mut frame = cfl::Frame::new();
            cfl::Trajectory::open(file, 'r')
                .unwrap()
                .read(&mut frame)
                .unwrap();
            let nsgrid = nsgrid::NSGrid::new(&frame, 2.45).unwrap();

            for i in 0..frame.size() {
                let mut nsg_result = nsgrid.get_neighbours(i).unwrap();
                nsg_result.sort_unstable();

                let mut selection = cfl::Selection::new(&*format!(
                    "two: distance(#1, #2) <= 2.45 and index(#1) == {}",
                    i
                ))
                .unwrap();
                let matches = selection.evaluate(&frame).unwrap();

                println!("{:?} {:?}", nsg_result, matches);

                assert!(!matches.is_empty() && nsgrid.has_neighbours(i).unwrap());
                assert_eq!(nsg_result.len(), matches.len());
            }
        }
    }

    #[test]
    fn pbc() {
        let mut frame = cfl::Frame::new();
        frame.set_cell(&cfl::UnitCell::new([10.0, 10.0, 10.0]));

        frame.add_atom(&cfl::Atom::new("H"), [1.0, 1.0, 9.0], None);
        frame.add_atom(&cfl::Atom::new("H"), [9.0, 1.0, 9.0], None);
        frame.add_atom(&cfl::Atom::new("H"), [9.0, 1.0, 1.0], None);
        frame.add_atom(&cfl::Atom::new("H"), [9.0, 9.0, 1.0], None);

        {
            frame.set("pbc_x", true);
            frame.set("pbc_y", true);
            frame.set("pbc_z", true);
            let nsgrid = nsgrid::NSGrid::new(&frame, 4.0).unwrap();
            let mut nsg_result = nsgrid.get_neighbours(0).unwrap();
            nsg_result.sort_unstable();
            assert_eq!(nsg_result.len(), 3);
            assert_eq!(nsg_result[0], 1);
            assert_eq!(nsg_result[1], 2);
            assert_eq!(nsg_result[2], 3);
        }

        {
            frame.set("pbc_x", true);
            frame.set("pbc_y", false);
            frame.set("pbc_z", false);
            let nsgrid = nsgrid::NSGrid::new(&frame, 4.0).unwrap();
            let nsg_result = nsgrid.get_neighbours(0).unwrap();
            assert_eq!(nsg_result.len(), 1);
            assert_eq!(nsg_result[0], 1);
        }

        {
            frame.set("pbc_x", false);
            frame.set("pbc_y", true);
            frame.set("pbc_z", false);
            let nsgrid = nsgrid::NSGrid::new(&frame, 4.0).unwrap();
            let nsg_result = nsgrid.get_neighbours(2).unwrap();
            assert_eq!(nsg_result.len(), 1);
            assert_eq!(nsg_result[0], 3);
        }

        {
            frame.set("pbc_x", false);
            frame.set("pbc_y", false);
            frame.set("pbc_z", true);
            let nsgrid = nsgrid::NSGrid::new(&frame, 4.0).unwrap();
            let nsg_result = nsgrid.get_neighbours(2).unwrap();
            assert_eq!(nsg_result.len(), 1);
            assert_eq!(nsg_result[0], 1);
        }

        frame.add_atom(&cfl::Atom::new("H"), [1.0, 1.0, 11.0], None);
        {
            frame.set("pbc_x", false);
            frame.set("pbc_y", false);
            frame.set("pbc_z", false);
            let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();
            let mut nsg_result = nsgrid.get_neighbours(0).unwrap();
            nsg_result.sort_unstable();
            assert_eq!(nsg_result.len(), 1);
            assert_eq!(nsg_result[0], 4);
        }
    }
}

#[cfg(all(feature = "from-chemfiles", test))]
mod into_chemfiles {
    use super::*;

    #[test]
    fn frame() {
        let cell = nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true, false, true]);
        let positions = vec![
            [1.0, 1.0, 9.0],
            [9.0, 1.0, 9.0],
            [9.0, 1.0, 1.0],
            [9.0, 9.0, 1.0],
        ];
        let frame = nsgrid::frame::Frame::new(cell, positions);
        let cfl_frame = cfl::Frame::from(&frame);

        assert_eq!(cfl_frame.cell().shape(), cfl::CellShape::Orthorhombic);
        assert_eq!(cfl_frame.get("pbc_x"), Some(cfl::Property::Bool(true)));
        assert_eq!(cfl_frame.get("pbc_y"), Some(cfl::Property::Bool(false)));
        assert_eq!(cfl_frame.get("pbc_z"), Some(cfl::Property::Bool(true)));
        assert_ulps_eq!(cfl_frame.positions()[0].as_ref(), [1.0, 1.0, 9.0].as_ref());
        assert_ulps_eq!(cfl_frame.positions()[1].as_ref(), [9.0, 1.0, 9.0].as_ref());
        assert_ulps_eq!(cfl_frame.positions()[2].as_ref(), [9.0, 1.0, 1.0].as_ref());
        assert_ulps_eq!(cfl_frame.positions()[3].as_ref(), [9.0, 9.0, 1.0].as_ref());
    }
}

#[cfg(all(feature = "from-chemfiles", test))]
mod cell_conversion_chemfiles {
    use super::*;

    #[test]
    fn from_chemfiles() {
        {
            let cfl_othorhombic = cfl::UnitCell::new([10.0, 20.0, 30.0]);
            let orthorhombic = nsgrid::cell::Cell::from_chemfiles_pbc(&cfl_othorhombic, [true; 3]);
            assert_eq!(*orthorhombic.shape(), nsgrid::cell::Shape::Orthorhombic);
            assert_ulps_eq!(
                cfl_othorhombic.lengths()[..],
                orthorhombic.lengths().as_ref()
            );
            assert_ulps_eq!(cfl_othorhombic.angles()[..], orthorhombic.angles().as_ref());
            assert_ulps_eq!(cfl_othorhombic.volume(), orthorhombic.volume());
        }

        {
            let cfl_triclinic = cfl::UnitCell::triclinic([10.0, 20.0, 30.0], [80.0, 70.0, 60.0]);
            let triclinic = nsgrid::cell::Cell::from_chemfiles_pbc(&cfl_triclinic, [true; 3]);
            assert_eq!(*triclinic.shape(), nsgrid::cell::Shape::Triclinic);
            assert_ulps_eq!(cfl_triclinic.lengths()[..], triclinic.lengths().as_ref());
            assert_ulps_eq!(cfl_triclinic.angles()[..], triclinic.angles().as_ref());
            assert_ulps_eq!(cfl_triclinic.volume(), triclinic.volume());
        }

        {
            let cfl_infinite = cfl::UnitCell::infinite();
            let infinite = nsgrid::cell::Cell::from_chemfiles_pbc(&cfl_infinite, [false; 3]);
            assert_eq!(*infinite.shape(), nsgrid::cell::Shape::Infinite);
            assert_ulps_eq!(cfl_infinite.lengths()[..], infinite.lengths().as_ref());
            assert_ulps_eq!(cfl_infinite.angles()[..], infinite.angles().as_ref());
            assert_ulps_eq!(cfl_infinite.volume(), infinite.volume());
        }
    }

    #[test]
    fn into_chemfiles() {
        {
            let orthorhombic = nsgrid::cell::Cell::orthorhombic([10.0, 20.0, 30.0], [true; 3]);
            let cfl_othorhombic = cfl::UnitCell::from(&orthorhombic);
            assert_eq!(cfl_othorhombic.shape(), cfl::CellShape::Orthorhombic);
            assert_ulps_eq!(
                cfl_othorhombic.lengths()[..],
                orthorhombic.lengths().as_ref()
            );
            assert_ulps_eq!(cfl_othorhombic.angles()[..], orthorhombic.angles().as_ref());
            assert_ulps_eq!(cfl_othorhombic.volume(), orthorhombic.volume());
        }

        {
            let triclinic =
                nsgrid::cell::Cell::triclinic([10.0, 20.0, 30.0], [80.0, 70.0, 60.0], [true; 3]);
            let cfl_triclinic = cfl::UnitCell::from(&triclinic);
            assert_eq!(cfl_triclinic.shape(), cfl::CellShape::Triclinic);
            assert_ulps_eq!(cfl_triclinic.lengths()[..], triclinic.lengths().as_ref());
            assert_ulps_eq!(cfl_triclinic.angles()[..], triclinic.angles().as_ref());
            assert_ulps_eq!(cfl_triclinic.volume(), triclinic.volume());
        }

        {
            let infinite = nsgrid::cell::Cell::infinite();
            let cfl_infinite = cfl::UnitCell::from(&infinite);
            assert_eq!(cfl_infinite.shape(), cfl::CellShape::Infinite);
            assert_ulps_eq!(cfl_infinite.lengths()[..], infinite.lengths().as_ref());
            assert_ulps_eq!(cfl_infinite.angles()[..], infinite.angles().as_ref());
            assert_ulps_eq!(cfl_infinite.volume(), infinite.volume());
        }
    }
}
