import numpy as np
from chemfiles import Frame, Atom, UnitCell, Trajectory

n = 1000
lx = 10
ly = 20
lz = 30

frame = Frame()
for _ in range(n):
    frame.add_atom(Atom("A"), np.random.rand(3) * [lx, ly, lz])
    frame.add_atom(Atom("B"), np.random.rand(3) * [lx, ly, lz])

frame.cell = UnitCell(lx, ly, lz)

for i in range(len(frame.positions)):
    frame.positions[i] = frame.cell.wrap(frame.positions[i])

trajectory = Trajectory("packed_orthorhombic_hd.gro", 'w')
trajectory.write(frame)
trajectory.close()


alpha = 30
beta = 45
gamma = 60

frame = Frame()
for _ in range(n):
    frame.add_atom(Atom("A"), np.random.rand(3) * [lx, ly, lz])
    frame.add_atom(Atom("B"), np.random.rand(3) * [lx, ly, lz])

frame.cell = UnitCell(lx, ly, lz, alpha, beta, gamma)

for i in range(len(frame.positions)):
    frame.positions[i] = frame.cell.wrap(frame.positions[i])

trajectory = Trajectory("packed_triclinic_hd.gro", 'w')
trajectory.write(frame)
trajectory.close()


frame = Frame()
for _ in range(n):
    frame.add_atom(Atom("A"), np.random.rand(3) * [lx, ly, lz])
    frame.add_atom(Atom("B"), np.random.rand(3) * [lx, ly, lz])

trajectory = Trajectory("infinite_hd.gro", 'w')
trajectory.write(frame)
trajectory.close()


n = 100


frame = Frame()
for _ in range(n):
    frame.add_atom(Atom("A"), (np.random.rand(3) - 0.5) * [lx, ly, lz])
    frame.add_atom(Atom("B"), (np.random.rand(3) - 0.5) * [lx, ly, lz])

frame.cell = UnitCell(lx, ly, lz)

for i in range(len(frame.positions)):
    frame.positions[i] = frame.cell.wrap(frame.positions[i])

trajectory = Trajectory("packed_orthorhombic_ld_neg.gro", 'w')
trajectory.write(frame)
trajectory.close()


alpha = 30
beta = 45
gamma = 60

frame = Frame()
for _ in range(n):
    frame.add_atom(Atom("A"), (np.random.rand(3) - 0.5) * [lx, ly, lz])
    frame.add_atom(Atom("B"), (np.random.rand(3) - 0.5) * [lx, ly, lz])

frame.cell = UnitCell(lx, ly, lz, alpha, beta, gamma)

for i in range(len(frame.positions)):
    frame.positions[i] = frame.cell.wrap(frame.positions[i])

trajectory = Trajectory("packed_triclinic_ld_neg.gro", 'w')
trajectory.write(frame)
trajectory.close()


frame = Frame()
for _ in range(n):
    frame.add_atom(Atom("A"), (np.random.rand(3) - 0.5) * [lx, ly, lz])
    frame.add_atom(Atom("B"), (np.random.rand(3) - 0.5) * [lx, ly, lz])

trajectory = Trajectory("infinite_ld_neg.gro", 'w')
trajectory.write(frame)
trajectory.close()
