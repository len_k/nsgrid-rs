extern crate nsgrid;
use chemfiles as cfl;
use std::time::Instant;

fn main() {
    let mut frame = cfl::Frame::new();
    cfl::Trajectory::open("tests/packed_orthorhombic_hd.gro", 'r')
        .unwrap()
        .read(&mut frame)
        .unwrap();

    let now = Instant::now();
    let nsgrid = nsgrid::NSGrid::new(&frame, 3.0).unwrap();
    println!("nsgrid setup:\t{} ns", now.elapsed().as_nanos());

    let mut counter = 0;
    let mut nsgrid_hn_time = 0;
    let mut nsgrid_time = 0;
    let mut cfl_time = 0;

    for i in 0..*[10, frame.size()].iter().min().unwrap() {
        let now = Instant::now();
        nsgrid.has_neighbours(i).unwrap();
        nsgrid_hn_time += now.elapsed().as_nanos();

        let now = Instant::now();
        nsgrid.get_neighbours(i).unwrap();
        nsgrid_time += now.elapsed().as_nanos();

        let mut selection = cfl::Selection::new(&*format!(
            "two: distance(#1, #2) <= 3.0 and index(#1) == {}",
            i
        ))
        .unwrap();

        let now = Instant::now();
        selection.evaluate(&frame).unwrap();
        cfl_time += now.elapsed().as_nanos();

        counter += 1;
    }

    println!("nsgrid hn:\t{} ns", nsgrid_hn_time / counter);
    println!("nsgrid:\t\t{} ns", nsgrid_time / counter);
    println!("chemfiles:\t{} ns", cfl_time / counter);
}
