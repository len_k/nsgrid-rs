use crate::cell;
use crate::types::{NSGridError, Position};
use std::convert::TryFrom;

#[derive(Clone)]
pub(crate) struct BoundingBox {
    origin: Position,
    lengths: [f64; 3],
}

impl BoundingBox {
    fn from_positions(positions: &[Position]) -> Self {
        if positions.is_empty() {
            return Self {
                origin: [0.0; 3],
                lengths: [0.0; 3],
            };
        }

        let mut min = [std::f64::MAX; 3];
        let mut max = [std::f64::MIN; 3];
        positions.iter().for_each(|position| {
            (0..3).for_each(|i| {
                if position[i] < min[i] {
                    min[i] = position[i]
                }
                if position[i] > max[i] {
                    max[i] = position[i]
                }
            });
        });
        // use fudge factor, so that all atoms are certainly within the bounding box
        BoundingBox {
            origin: min,
            lengths: [
                max[0] - min[0] + 1e-5,
                max[1] - min[1] + 1e-5,
                max[2] - min[2] + 1e-5,
            ],
        }
    }

    pub fn from_positions_cell(positions: &[Position], cell: &cell::Cell) -> Self {
        if *cell.shape() == cell::Shape::Infinite {
            BoundingBox::from_positions(positions)
        } else {
            let matrix = cell.matrix();
            let mut origin = [0.0; 3];
            let mut cartesian_lengths = [matrix[0][0], matrix[1][1], matrix[2][2]];
            debug_assert!(
                (cell.volume()
                    - cartesian_lengths[0] * cartesian_lengths[1] * cartesian_lengths[2])
                    .abs()
                    <= 1e-5
            );
            if cell.periodic().iter().any(|x| !*x) {
                let bbox = BoundingBox::from_positions(positions);
                (0..3).filter(|i| !cell.periodic()[*i]).for_each(|i| {
                    if bbox.origin[i] + bbox.lengths[i] > cartesian_lengths[i] {
                        cartesian_lengths[i] = bbox.lengths[i];
                    } else {
                        cartesian_lengths[i] +=
                            cartesian_lengths[i] - (bbox.origin[i] + bbox.lengths[i]);
                    }
                    if bbox.origin[i] < origin[i] {
                        origin[i] = bbox.origin[i];
                    } else {
                        cartesian_lengths[i] += bbox.origin[i] - origin[i];
                    }
                });
            }
            Self {
                origin,
                lengths: cartesian_lengths,
            }
        }
    }

    pub fn check_cutoff(
        &self,
        periodic: [bool; 3],
        cutoff: f64,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut min_periodic_length = None;
        self.lengths()
            .iter()
            .zip(periodic.iter())
            .filter_map(|(l, &p)| if p { Some(l) } else { None })
            .for_each(|l| {
                if let Some(mpl) = min_periodic_length {
                    if l < mpl {
                        min_periodic_length = Some(l);
                    }
                } else {
                    min_periodic_length = Some(l);
                }
            });
        if let Some(&mpl) = min_periodic_length {
            if cutoff > mpl {
                return Err(Box::new(NSGridError(
                    "cutoff must be less or equal to the minimal cartesian length of the cell"
                        .to_string(),
                )));
            }
        }
        Ok(())
    }

    pub fn calc_num_cells(&self, cutoff: f64) -> Result<[usize; 3], Box<dyn std::error::Error>> {
        #[allow(clippy::cast_possible_truncation)]
        let mut num_cells = self
            .lengths
            .iter()
            .map(|l| usize::try_from((l / cutoff).floor() as isize))
            .collect::<Result<Vec<_>, _>>()?;
        num_cells
            .iter_mut()
            .filter(|l| **l == 0)
            .for_each(|l| *l = 1);
        Ok([num_cells[0], num_cells[1], num_cells[2]])
    }

    pub fn calc_subcell_lengths(&self, num_cells: &[usize; 3]) -> [f64; 3] {
        #[allow(clippy::cast_precision_loss)]
        let subcell_lengths = self
            .lengths
            .iter()
            .zip(num_cells.iter())
            .map(|(l, &n)| l / n as f64)
            .collect::<Vec<_>>();
        [subcell_lengths[0], subcell_lengths[1], subcell_lengths[2]]
    }

    pub fn shift(&self, position: &Position) -> Position {
        [
            position[0] - self.origin[0],
            position[1] - self.origin[1],
            position[2] - self.origin[2],
        ]
    }

    pub fn lengths(&self) -> &[f64; 3] {
        &self.lengths
    }

    pub fn contains(&self, position: &Position, periodic: [bool; 3]) -> bool {
        !(0..3).any(|i| {
            !periodic[i]
                && (position[i] < self.origin[i]
                    || position[i] > self.origin[i] + self.lengths[i] - 1e-5)
        })
    }
}
