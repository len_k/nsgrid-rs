use std::fmt;

#[derive(Debug)]
pub struct NSGridError(pub String);

impl fmt::Display for NSGridError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "NSGrid encountered an error: {}", self.0)
    }
}

impl std::error::Error for NSGridError {}

pub type Position = [f64; 3];
