//! The `frame` mod contains `Frame`.
use crate::cell;
use crate::types::Position;
#[cfg(feature = "from-chemfiles")]
use chemfiles as cfl;

/// A `Frame` contains a `Cell` and all particle positions.
#[derive(Clone)]
pub struct Frame {
    cell: cell::Cell,
    positions: Vec<Position>,
}

impl Frame {
    /// Creates a `Frame` with given `cell` and `positions`.
    #[must_use]
    pub fn new(cell: cell::Cell, positions: Vec<Position>) -> Self {
        Self { cell, positions }
    }

    /// Returns the number of particle positions.
    #[must_use]
    pub fn size(&self) -> usize {
        self.positions.len()
    }

    /// Returns a reference to the particle positions.
    #[must_use]
    pub fn positions(&self) -> &[Position] {
        &self.positions
    }

    /// Returns a mutable reference to the particle positions.
    #[must_use]
    pub fn positions_mut(&mut self) -> &mut [Position] {
        &mut self.positions
    }

    /// Adds a new position and returns the corresponding index.
    pub fn add_position(&mut self, new_position: Position) -> usize {
        self.positions.push(new_position);
        self.size() - 1
    }

    /// Returns a reference to the cell.
    #[must_use]
    pub fn cell(&self) -> &cell::Cell {
        &self.cell
    }

    /// Calculates the squared distance between positions `pos_i` and `pos_j`.
    #[must_use]
    pub fn distance_positions_sq(&self, pos_i: &Position, pos_j: &Position) -> f64 {
        let mut v = [
            pos_i[0] - pos_j[0],
            pos_i[1] - pos_j[1],
            pos_i[2] - pos_j[2],
        ];
        self.cell.wrap(&mut v);
        v.iter().map(|d| d.powi(2)).sum()
    }

    /// Calculates the squared distance between index `i` and position `pos_j`.
    #[must_use]
    pub fn distance_index_position_sq(&self, i: usize, pos_j: &Position) -> f64 {
        assert!(
            i < self.size(),
            "Atomic index out of bounds: there are {} atoms, but index {} was given",
            self.size(),
            i,
        );

        self.distance_positions_sq(&self.positions[i], pos_j)
    }

    /// Calculates the squared distance between indices `i` and `j`.
    #[must_use]
    pub fn distance_sq(&self, i: usize, j: usize) -> f64 {
        assert!(
            i < self.size() && j < self.size(),
            "Atomic index out of bounds: there are {} atoms, but indices {} and {} were given",
            self.size(),
            i,
            j
        );

        self.distance_positions_sq(&self.positions[i], &self.positions[j])
    }
}

#[cfg(feature = "from-chemfiles")]
impl From<&cfl::Frame> for Frame {
    fn from(cfl_frame: &cfl::Frame) -> Self {
        let frame_pbc = [
            cfl_frame.get("pbc_x"),
            cfl_frame.get("pbc_y"),
            cfl_frame.get("pbc_z"),
        ];
        let has_pbc = frame_pbc.iter().map(Option::is_some).any(|x| x);
        let periodic = if cfl_frame.cell().shape() == cfl::CellShape::Infinite {
            if has_pbc {
                eprintln!("Ignore PBC property for infinite frame");
            }
            [false; 3]
        } else {
            let pbc: Vec<_> = frame_pbc
                .iter()
                .map(|d| {
                    if let Some(d) = d {
                        if let cfl::Property::Bool(x) = d {
                            *x
                        } else {
                            eprintln!("Using `true` as default for non-bool PBC property");
                            true
                        }
                    } else {
                        eprintln!("Using `true` as default for missing PBC direction");
                        true
                    }
                })
                .collect();
            [pbc[0], pbc[1], pbc[2]]
        };
        let cell = cell::Cell::from_chemfiles_pbc(&*cfl_frame.cell(), periodic);
        Self {
            cell,
            positions: Vec::from(cfl_frame.positions()),
        }
    }
}

#[cfg(feature = "from-chemfiles")]
impl From<&Frame> for cfl::Frame {
    fn from(frame: &Frame) -> Self {
        let cfl_cell = cfl::UnitCell::from(frame.cell());
        let mut cfl_frame = cfl::Frame::new();
        cfl_frame.set_cell(&cfl_cell);

        let frame_pbc = frame.cell().periodic();
        cfl_frame.set("pbc_x", frame_pbc[0]);
        cfl_frame.set("pbc_y", frame_pbc[1]);
        cfl_frame.set("pbc_z", frame_pbc[2]);

        cfl_frame.resize(frame.size());
        let cfl_position = cfl_frame.positions_mut();
        frame
            .positions()
            .iter()
            .cloned()
            .enumerate()
            .for_each(|(i, pos)| cfl_position[i] = pos);

        cfl_frame
    }
}
