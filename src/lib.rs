//! `nsgrid-rs` is a fast fixed-radius near neighbour search implementation for chemfiles based on
//! a cell list algorithm.

#![deny(missing_docs)]
// Configuration for clippy lints
#![warn(clippy::all, clippy::pedantic)]

use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::TryFrom;
pub mod frame;
use frame::Frame;
pub mod cell;
mod types;
use crate::types::{NSGridError, Position};
mod bounding_box;
use bounding_box::BoundingBox;

enum Probe<'a> {
    Index(usize),
    Position(&'a Position),
}

/// A `NSGrid` contains a `Frame`. This frame is partitioned into subcells which hold indices
/// of their assigned atoms. The subcells are orthorhombic and their cell lengths are greater or
/// equal to the defined `cutoff`.
pub struct NSGrid {
    frame: Frame,
    cutoff_sq: f64,
    bounding_box: BoundingBox,
    num_cells: [usize; 3],
    dirs: [Vec<i64>; 3],
    cells: Vec<Vec<Vec<HashSet<usize>>>>,
    subcell_lengths: [f64; 3],
    cell_index_lookup: HashMap<usize, [usize; 3]>,
}

impl NSGrid {
    /// Creates a new `NSGrid` with a given cutoff from a `Frame`.
    /// The Frame can have `pbc_{x,y,z}` as bool properties.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `cutoff` is too large with respect to box dimensions
    /// or if `usize` overflows.
    pub fn new<T: Into<Frame>>(frame: T, cutoff: f64) -> Result<Self, Box<dyn std::error::Error>> {
        let empty_preselection: Option<Vec<usize>> = None;
        Self::construct(frame, cutoff, empty_preselection)
    }

    /// Creates a new `NSGrid` with a given cutoff and a preselection from a `Frame`.
    /// The Frame can have `pbc_{x,y,z}` as bool properties.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `cutoff` is too large with respect to box dimensions
    /// or if `usize` overflows.
    pub fn with_selection<T: Into<Frame>>(
        frame: T,
        cutoff: f64,
        selection: impl std::iter::IntoIterator<Item = usize>,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        Self::construct(frame, cutoff, Some(selection))
    }

    fn construct<T: Into<Frame>>(
        cfl_frame: T,
        cutoff: f64,
        selection: Option<impl std::iter::IntoIterator<Item = usize>>,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let frame = cfl_frame.into();
        let bounding_box = BoundingBox::from_positions_cell(frame.positions(), frame.cell());
        let periodic = *frame.cell().periodic();
        bounding_box.check_cutoff(periodic, cutoff)?;

        let num_cells = bounding_box.calc_num_cells(cutoff)?;

        let subcell_lengths = bounding_box.calc_subcell_lengths(&num_cells);
        debug_assert!(
            (subcell_lengths[0] >= cutoff || !periodic[0])
                && (subcell_lengths[1] >= cutoff || !periodic[1])
                && (subcell_lengths[2] >= cutoff || !periodic[2])
        );

        let mut nsgrid = Self {
            frame,
            cutoff_sq: cutoff.powi(2),
            bounding_box,
            num_cells,
            dirs: calc_directions(&num_cells),
            cells: vec![vec![vec![HashSet::new(); num_cells[2]]; num_cells[1]]; num_cells[0]],
            subcell_lengths,
            cell_index_lookup: HashMap::new(),
        };

        match selection {
            Some(sel) => {
                for atom_idx in sel {
                    nsgrid.index(atom_idx)?;
                }
            }
            None => {
                for atom_idx in 0..nsgrid.frame.size() {
                    nsgrid.index(atom_idx)?;
                }
            }
        }

        Ok(nsgrid)
    }

    fn index(&mut self, atom_idx: usize) -> Result<(), Box<dyn std::error::Error>> {
        let cell_idx = self.calc_cell_index(atom_idx)?;
        self.cells[cell_idx[0]][cell_idx[1]][cell_idx[2]].insert(atom_idx);
        self.cell_index_lookup.insert(atom_idx, cell_idx);
        Ok(())
    }

    fn calc_cell_index_position(
        &self,
        position: &Position,
    ) -> Result<[usize; 3], std::num::TryFromIntError> {
        let shifted_pos = self.bounding_box.shift(position);
        let cell_idx = shifted_pos
            .iter()
            .zip(self.num_cells.iter())
            .zip(self.subcell_lengths.iter())
            .map(|((x, &n), l)| {
                #[allow(clippy::cast_precision_loss)]
                let i = (x / l).floor() % n as f64;
                if i < 0.0 {
                    #[allow(clippy::cast_possible_truncation, clippy::cast_precision_loss)]
                    usize::try_from((n as f64 + i) as isize)
                } else {
                    #[allow(clippy::cast_possible_truncation)]
                    usize::try_from(i as isize)
                }
            })
            .collect::<Result<Vec<_>, _>>()?;
        Ok([cell_idx[0], cell_idx[1], cell_idx[2]])
    }

    fn calc_cell_index(&self, atom_idx: usize) -> Result<[usize; 3], std::num::TryFromIntError> {
        self.calc_cell_index_position(&self.frame.positions()[atom_idx])
    }

    fn calc_cell_index_direction(
        &self,
        probe_cell_idx: &[usize; 3],
        direction: &[i64; 3],
    ) -> Result<Option<[usize; 3]>, std::num::TryFromIntError> {
        let mut cell_idx = [0; 3];
        for i in 0..3 {
            let periodic = self.frame.cell().periodic();
            let n = probe_cell_idx[i] as i64 + direction[i];
            cell_idx[i] = if n < 0 {
                if periodic[i] {
                    usize::try_from(self.num_cells[i] as i64 + n)?
                } else {
                    return Ok(None);
                }
            } else if n >= self.num_cells[i] as i64 {
                if periodic[i] {
                    usize::try_from(n - self.num_cells[i] as i64)?
                } else {
                    return Ok(None);
                }
            } else {
                usize::try_from(n)?
            };
        }
        Ok(Some(cell_idx))
    }

    fn get_neighbours_in_cell_index(
        &self,
        probe_idx: usize,
        probe_cell_idx: &[usize; 3],
        direction: &[i64; 3],
    ) -> Result<Vec<usize>, std::num::TryFromIntError> {
        if let Some(cell_idx) = self.calc_cell_index_direction(probe_cell_idx, direction)? {
            return Ok(self.cells[cell_idx[0]][cell_idx[1]][cell_idx[2]]
                .iter()
                .filter(|&&idx| {
                    (self.frame.distance_sq(probe_idx, idx) <= self.cutoff_sq) && (idx != probe_idx)
                })
                .cloned()
                .collect());
        }
        Ok(vec![])
    }

    fn get_neighbours_in_cell_position(
        &self,
        probe_pos: &Position,
        probe_cell_idx: &[usize; 3],
        direction: &[i64; 3],
    ) -> Result<Vec<usize>, std::num::TryFromIntError> {
        if let Some(cell_idx) = self.calc_cell_index_direction(probe_cell_idx, direction)? {
            return Ok(self.cells[cell_idx[0]][cell_idx[1]][cell_idx[2]]
                .iter()
                .filter(|&&idx| {
                    self.frame.distance_index_position_sq(idx, probe_pos) <= self.cutoff_sq
                })
                .cloned()
                .collect());
        }
        Ok(vec![])
    }

    fn get_probe_neighbours(
        &self,
        probe: &Probe,
    ) -> Result<Vec<usize>, Box<dyn std::error::Error>> {
        let mut neighbours = Vec::new();
        match probe {
            Probe::Index(probe_idx) => {
                let probe_cell_idx = self.cell_index_lookup.get(probe_idx).ok_or_else(|| {
                    Box::new(NSGridError(format!(
                        "atom index {} is not in the lookup table",
                        probe_idx
                    )))
                })?;
                for nx in &self.dirs[0] {
                    for ny in &self.dirs[1] {
                        for nz in &self.dirs[2] {
                            neighbours.extend(self.get_neighbours_in_cell_index(
                                *probe_idx,
                                &probe_cell_idx,
                                &[*nx, *ny, *nz],
                            )?);
                        }
                    }
                }
            }
            Probe::Position(probe_pos) => {
                let probe_cell_idx = self.calc_cell_index_position(probe_pos)?;
                for nx in &self.dirs[0] {
                    for ny in &self.dirs[1] {
                        for nz in &self.dirs[2] {
                            neighbours.extend(self.get_neighbours_in_cell_position(
                                probe_pos,
                                &probe_cell_idx,
                                &[*nx, *ny, *nz],
                            )?);
                        }
                    }
                }
            }
        }
        Ok(neighbours)
    }

    /// Returns a list of atom idices that are a distance `cutoff` around `atom_idx`.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `atom_idx` is not in the lookup table
    /// or if `usize` overflows.
    pub fn get_neighbours(
        &self,
        atom_idx: usize,
    ) -> Result<Vec<usize>, Box<dyn std::error::Error>> {
        self.get_probe_neighbours(&Probe::Index(atom_idx))
    }

    /// Returns a list of atom idices that are a distance `cutoff` around `position`.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `usize` overflows.
    pub fn get_position_neighbours(
        &self,
        position: &Position,
    ) -> Result<Vec<usize>, Box<dyn std::error::Error>> {
        self.get_probe_neighbours(&Probe::Position(position))
    }

    fn has_neighbours_in_cell_index(
        &self,
        probe_idx: usize,
        probe_cell_idx: &[usize; 3],
        direction: &[i64; 3],
    ) -> Result<bool, std::num::TryFromIntError> {
        if let Some(cell_idx) = self.calc_cell_index_direction(probe_cell_idx, direction)? {
            return Ok(self.cells[cell_idx[0]][cell_idx[1]][cell_idx[2]]
                .iter()
                .any(|&idx| {
                    (self.frame.distance_sq(probe_idx, idx) <= self.cutoff_sq) && (idx != probe_idx)
                }));
        }
        Ok(false)
    }

    fn has_neighbours_in_cell_position(
        &self,
        probe_pos: &Position,
        probe_cell_idx: &[usize; 3],
        direction: &[i64; 3],
    ) -> Result<bool, std::num::TryFromIntError> {
        if let Some(cell_idx) = self.calc_cell_index_direction(probe_cell_idx, direction)? {
            return Ok(self.cells[cell_idx[0]][cell_idx[1]][cell_idx[2]]
                .iter()
                .any(|&idx| {
                    self.frame.distance_index_position_sq(idx, probe_pos) <= self.cutoff_sq
                }));
        }
        Ok(false)
    }

    fn has_probe_neighbours(&self, probe: &Probe) -> Result<bool, Box<dyn std::error::Error>> {
        match probe {
            Probe::Index(probe_idx) => {
                let probe_cell_idx = self.cell_index_lookup.get(probe_idx).ok_or_else(|| {
                    Box::new(NSGridError(format!(
                        "atom index {} is not in the lookup table",
                        probe_idx
                    )))
                })?;
                for nx in &self.dirs[0] {
                    for ny in &self.dirs[1] {
                        for nz in &self.dirs[2] {
                            if self.has_neighbours_in_cell_index(
                                *probe_idx,
                                &probe_cell_idx,
                                &[*nx, *ny, *nz],
                            )? {
                                return Ok(true);
                            }
                        }
                    }
                }
            }
            Probe::Position(probe_pos) => {
                let probe_cell_idx = self.calc_cell_index_position(probe_pos)?;
                for nx in &self.dirs[0] {
                    for ny in &self.dirs[1] {
                        for nz in &self.dirs[2] {
                            if self.has_neighbours_in_cell_position(
                                probe_pos,
                                &probe_cell_idx,
                                &[*nx, *ny, *nz],
                            )? {
                                return Ok(true);
                            }
                        }
                    }
                }
            }
        }
        Ok(false)
    }

    /// Returns `true` if at least one atom is a distance `cutoff` around `atom_idx`.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `atom_idx` is not in the lookup table
    /// or if `usize` overflows.
    pub fn has_neighbours(&self, atom_idx: usize) -> Result<bool, Box<dyn std::error::Error>> {
        self.has_probe_neighbours(&Probe::Index(atom_idx))
    }

    /// Returns `true` if at least one atom is a distance `cutoff` around `position`.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `usize` overflows.
    pub fn has_position_neighbours(
        &self,
        position: &Position,
    ) -> Result<bool, Box<dyn std::error::Error>> {
        self.has_probe_neighbours(&Probe::Position(position))
    }

    /// Returns the `frame`
    #[must_use]
    pub fn frame(&self) -> &Frame {
        &self.frame
    }

    /// Update the position of an already indexed particle.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `atom_idx` is not in the lookup table,
    /// if `usize` overflows or if `new_position` is outside of the bounding box.
    pub fn update_position(
        &mut self,
        atom_idx: usize,
        new_position: Position,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let cell_idx = self.cell_index_lookup.get(&atom_idx).ok_or_else(|| {
            Box::new(NSGridError(format!(
                "atom index {} is not in the lookup table",
                atom_idx
            )))
        })?;
        if !self
            .bounding_box
            .contains(&new_position, *self.frame.cell().periodic())
        {
            return Err(Box::new(NSGridError(
                "New Position outside of bounding box".to_string(),
            )));
        }
        self.frame.positions_mut()[atom_idx] = new_position;
        self.cells[cell_idx[0]][cell_idx[1]][cell_idx[2]].remove(&atom_idx);
        self.index(atom_idx)?;
        Ok(())
    }

    /// Add a new position.
    ///
    /// # Errors
    ///
    /// Will return `Err` if `new_position` is outside of the bounding box.
    pub fn add_position(
        &mut self,
        new_position: Position,
    ) -> Result<(), Box<dyn std::error::Error>> {
        if !self
            .bounding_box
            .contains(&new_position, *self.frame.cell().periodic())
        {
            return Err(Box::new(NSGridError(
                "New Position outside of bounding box".to_string(),
            )));
        }
        let atom_idx = self.frame.add_position(new_position);
        self.index(atom_idx)?;
        Ok(())
    }
}

fn calc_directions(num_cells: &[usize; 3]) -> [Vec<i64>; 3] {
    let dirs: Vec<_> = num_cells
        .iter()
        .map(|&n| {
            if n == 1 {
                vec![0]
            } else if n == 2 {
                vec![0, 1]
            } else {
                vec![0, 1, -1]
            }
        })
        .collect();
    [dirs[0].clone(), dirs[1].clone(), dirs[2].clone()]
}
