//! The `cell` mod contains `Cell`.
#[cfg(feature = "from-chemfiles")]
use chemfiles as cfl;

/// Possible shapes of `Cell`.
#[derive(Clone, Debug, PartialEq)]
pub enum Shape {
    /// An orthorhombic cell.
    Orthorhombic,
    /// A triclinic cell.
    Triclinic,
    /// No cell.
    Infinite,
}

#[cfg(feature = "from-chemfiles")]
impl From<&cfl::CellShape> for Shape {
    fn from(cfl_shape: &cfl::CellShape) -> Self {
        match cfl_shape {
            cfl::CellShape::Orthorhombic => Shape::Orthorhombic,
            cfl::CellShape::Triclinic => Shape::Triclinic,
            cfl::CellShape::Infinite => Shape::Infinite,
        }
    }
}

#[cfg(feature = "from-chemfiles")]
impl From<&Shape> for cfl::CellShape {
    fn from(shape: &Shape) -> Self {
        match shape {
            Shape::Orthorhombic => cfl::CellShape::Orthorhombic,
            Shape::Triclinic => cfl::CellShape::Triclinic,
            Shape::Infinite => cfl::CellShape::Infinite,
        }
    }
}

/// A `Cell` contains a `chemfiles::CellShape`, the `lengths`, the `volume` and pbc information.
#[derive(Clone)]
pub struct Cell {
    shape: Shape,
    lengths: [f64; 3],
    angles: [f64; 3],
    matrix: [[f64; 3]; 3],
    inv_matrix: [[f64; 3]; 3],
    periodic: [bool; 3],
    pbc_all: bool,
    volume: f64,
}

impl Cell {
    /// Creates a orthorhombic `Cell` with given lengths and periodicity.
    #[must_use]
    pub fn orthorhombic(lengths: [f64; 3], periodic: [bool; 3]) -> Self {
        assert!(
            lengths.iter().all(|l| *l > 0.0),
            "Cell lengths must be positive"
        );

        let pbc_all = periodic.iter().all(|x| *x);
        let matrix = [
            [lengths[0], 0.0, 0.0],
            [0.0, lengths[1], 0.0],
            [0.0, 0.0, lengths[2]],
        ];
        let inv_matrix = mat_invert(&matrix);

        Self {
            shape: Shape::Orthorhombic,
            lengths,
            angles: [90.0; 3],
            matrix,
            inv_matrix,
            periodic,
            pbc_all,
            volume: lengths[0] * lengths[1] * lengths[2],
        }
    }

    /// Creates a triclinic `Cell` with given lengths and periodicity.
    #[must_use]
    pub fn triclinic(lengths: [f64; 3], angles: [f64; 3], periodic: [bool; 3]) -> Self {
        assert!(
            lengths.iter().all(|l| *l > 0.0),
            "Cell lengths must be positive"
        );
        assert!(
            angles.iter().all(|a| *a > 0.0) && angles.iter().all(|a| *a < 180.0),
            "Cell angles must be between 0 and 180"
        );

        let cos_alpha = angles[0].to_radians().cos();
        let cos_beta = angles[1].to_radians().cos();
        let (sin_gamma, cos_gamma) = angles[2].to_radians().sin_cos();

        let b_x = lengths[1] * cos_gamma;
        let b_y = lengths[1] * sin_gamma;

        let c_x = lengths[2] * cos_beta;
        let c_y = lengths[2] * (cos_alpha - cos_beta * cos_gamma) / sin_gamma;
        let c_z = (lengths[2] * lengths[2] - c_y * c_y - c_x * c_x).sqrt();

        let pbc_all = periodic.iter().all(|x| *x);
        let matrix = [[lengths[0], b_x, c_x], [0.0, b_y, c_y], [0.0, 0.0, c_z]];
        let inv_matrix = mat_invert(&matrix);

        let scale_volume =
            (1.0 - cos_alpha * cos_alpha - cos_beta * cos_beta - cos_gamma * cos_gamma
                + 2.0 * cos_alpha * cos_beta * cos_gamma)
                .sqrt();

        Self {
            shape: Shape::Triclinic,
            lengths,
            angles,
            matrix,
            inv_matrix,
            periodic,
            pbc_all,
            volume: lengths[0] * lengths[1] * lengths[2] * scale_volume,
        }
    }

    /// Creates an infinite `Cell`.
    #[must_use]
    pub fn infinite() -> Self {
        Self {
            shape: Shape::Infinite,
            lengths: [0.0; 3],
            angles: [90.0; 3],
            matrix: [[0.0; 3]; 3],
            inv_matrix: [[0.0; 3]; 3],
            periodic: [false; 3],
            pbc_all: false,
            volume: 0.0,
        }
    }

    /// Creates a `Cell` from a `chemfiles::UnitCell` with a given periodicity.
    #[cfg(feature = "from-chemfiles")]
    #[must_use]
    pub fn from_chemfiles_pbc(cfl_cell: &cfl::UnitCell, periodic: [bool; 3]) -> Self {
        match cfl_cell.shape() {
            cfl::CellShape::Orthorhombic => Self::orthorhombic(cfl_cell.lengths(), periodic),
            cfl::CellShape::Triclinic => {
                Self::triclinic(cfl_cell.lengths(), cfl_cell.angles(), periodic)
            }
            cfl::CellShape::Infinite => Self::infinite(),
        }
    }

    /// Returns a reference to the lengths.
    #[must_use]
    pub fn lengths(&self) -> &[f64; 3] {
        &self.lengths
    }

    /// Returns a reference to the angles.
    #[must_use]
    pub fn angles(&self) -> &[f64; 3] {
        &self.angles
    }

    /// Returns a reference to the periodicity in all directions.
    #[must_use]
    pub fn periodic(&self) -> &[bool; 3] {
        &self.periodic
    }

    /// Returns the volume.
    #[must_use]
    pub fn volume(&self) -> f64 {
        self.volume
    }

    /// Returns the shape.
    #[must_use]
    pub fn shape(&self) -> &Shape {
        &self.shape
    }

    /// Returns the matrix.
    #[must_use]
    pub fn matrix(&self) -> &[[f64; 3]] {
        &self.matrix
    }

    fn wrap_orthorhombic(&self, vector: &mut [f64; 3]) {
        vector
            .iter_mut()
            .zip(&self.lengths)
            .for_each(|(v, l)| *v -= (*v / l + 0.5).floor() * l);
    }

    fn wrap_pbc_orthorhombic(&self, vector: &mut [f64; 3]) {
        vector
            .iter_mut()
            .zip(&self.lengths)
            .zip(&self.periodic)
            .for_each(|((v, l), p)| {
                if *p {
                    *v -= (*v / l + 0.5).floor() * l
                }
            });
    }

    fn wrap_triclinic(&self, vector: &mut [f64; 3]) {
        let mut fractional = mat_dot_vec(&self.inv_matrix, vector);
        fractional[0] -= (fractional[0] + 0.5).floor();
        fractional[1] -= (fractional[1] + 0.5).floor();
        fractional[2] -= (fractional[2] + 0.5).floor();
        *vector = mat_dot_vec(&self.matrix, &fractional);
    }

    /// Wrap the `vector`.
    pub fn wrap(&self, vector: &mut [f64; 3]) {
        match self.shape {
            Shape::Infinite => (),
            Shape::Orthorhombic => {
                if self.pbc_all {
                    self.wrap_orthorhombic(vector)
                } else {
                    self.wrap_pbc_orthorhombic(vector)
                }
            }
            Shape::Triclinic => self.wrap_triclinic(vector),
        }
    }
}

#[cfg(feature = "from-chemfiles")]
impl From<&Cell> for cfl::UnitCell {
    fn from(cell: &Cell) -> Self {
        match cell.shape() {
            Shape::Orthorhombic => cfl::UnitCell::new(*cell.lengths()),
            Shape::Triclinic => cfl::UnitCell::triclinic(*cell.lengths(), *cell.angles()),
            Shape::Infinite => cfl::UnitCell::infinite(),
        }
    }
}

fn mat_determinant(matrix: &[[f64; 3]; 3]) -> f64 {
    let mut det: f64 = 0.0;
    det += matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2]);
    det -= matrix[0][1] * (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]);
    det += matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
    det
}

fn mat_invert(matrix: &[[f64; 3]; 3]) -> [[f64; 3]; 3] {
    let det = mat_determinant(matrix);
    assert!(det.abs() > 1e-6, "Matrix not invertible");
    let inv_det = 1.0 / det;

    let xx = (matrix[1][1] * matrix[2][2] - matrix[2][1] * matrix[1][2]) * inv_det;
    let xy = (matrix[0][2] * matrix[2][1] - matrix[0][1] * matrix[2][2]) * inv_det;
    let xz = (matrix[0][1] * matrix[1][2] - matrix[0][2] * matrix[1][1]) * inv_det;

    let yx = (matrix[1][2] * matrix[2][0] - matrix[1][0] * matrix[2][2]) * inv_det;
    let yy = (matrix[0][0] * matrix[2][2] - matrix[0][2] * matrix[2][0]) * inv_det;
    let yz = (matrix[1][0] * matrix[0][2] - matrix[0][0] * matrix[1][2]) * inv_det;

    let zx = (matrix[1][0] * matrix[2][1] - matrix[2][0] * matrix[1][1]) * inv_det;
    let zy = (matrix[2][0] * matrix[0][1] - matrix[0][0] * matrix[2][1]) * inv_det;
    let zz = (matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]) * inv_det;

    [[xx, xy, xz], [yx, yy, yz], [zx, zy, zz]]
}

fn mat_dot_vec(matrix: &[[f64; 3]; 3], vector: &[f64; 3]) -> [f64; 3] {
    [
        matrix[0][0] * vector[0] + matrix[0][1] * vector[1] + matrix[0][2] * vector[2],
        matrix[1][0] * vector[0] + matrix[1][1] * vector[1] + matrix[1][2] * vector[2],
        matrix[2][0] * vector[0] + matrix[2][1] * vector[1] + matrix[2][2] * vector[2],
    ]
}
